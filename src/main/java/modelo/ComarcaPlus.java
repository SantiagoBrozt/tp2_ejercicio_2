package modelo;

class ComarcaPlus extends Tarjeta {
	private double descuentoTotal = 0.02;
	
	private double aplicarDescuento(double costoBebidas, double costoPlatos) {
		double costoTotal = (costoBebidas + costoPlatos);
		costoTotal -= costoTotal * this.descuentoTotal;
		return costoTotal;
	}
	
	double pagar(double costoBebidas, double costoPLatos, double propina) throws Exception {
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		return costoTotal;
	}
}
