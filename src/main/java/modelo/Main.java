package modelo;

import java.time.LocalDate;
import java.util.function.Supplier;

import persistencia.EnDataBaseRegistroDeCobro;
import persistencia.EnDiscoRegistroDeCobro;

public class Main {
	public static void main(String[] args) {
		var proveedorDeFecha = new Supplier<LocalDate>() {
			@Override
			public LocalDate get() {
				return LocalDate.now();
			}
		};
		var mesa1 = new Mesa(1, new EnDiscoRegistroDeCobro("/home/sancho/Documentos/Facturaciones.txt"), proveedorDeFecha);
		var p1 = new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var visa = new Visa();
        var comarcaPlus = new ComarcaPlus();
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        try {
			var recibo = mesa1.cobrar(visa, 0.02);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        var mesa2 = new Mesa(2, new EnDataBaseRegistroDeCobro(), proveedorDeFecha);
        mesa2.pedirPlato(p1, 2);
        mesa2.pedirPlato(p2, 1);
        mesa2.pedirBedida(b1, 5);
        mesa2.pedirBedida(b2, 3);
        mesa2.confirmarPedido();
        try {
			var recibo = mesa2.cobrar(comarcaPlus, 0.02);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
