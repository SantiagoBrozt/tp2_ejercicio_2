package modelo;

class MasterCard extends Tarjeta {
	private double descuentoBebidas = 0;
	private double descuentoPlatos = 0.02;
	
	private double aplicarDescuento(double costoBebidas, double costoPlatos) {
		double costoTotal = this.aplicarDescuentoBebidas(costoBebidas) + aplicarDescuentoPlatos(costoPlatos);
		return costoTotal;
	}

	private double aplicarDescuentoBebidas (double costo) {
		costo -= costo * this.descuentoBebidas;
		return costo;
	}
	
	private double aplicarDescuentoPlatos (double costo) {
		costo -= costo * this.descuentoPlatos;
		return costo;
	}
	
	double pagar(double costoBebidas, double costoPLatos, double propina) throws Exception {
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		return costoTotal;
	}
}
