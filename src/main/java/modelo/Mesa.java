package modelo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

class Mesa {
	private final int numero;
	private static int ultimoReciboGenerado;
	private boolean pedidoConfirmado;
	private List<Bebida> bebidasPedidas;
	private List<PlatoPrincipal> platosPedidos;
	private RegistroDeCobro registrador;
	private Supplier<LocalDate> proveedorDeFecha;
 	
	
	Mesa(int numero, RegistroDeCobro registrador, Supplier<LocalDate> proveedorDeFecha) {
		this.numero = numero;
		this.pedidoConfirmado = false;
		this.bebidasPedidas = new ArrayList<>();
		this.platosPedidos = new ArrayList<>();
		this.registrador = registrador;
		this.proveedorDeFecha = proveedorDeFecha;
	}
	
	void pedirBedida(Bebida bebida, int cantidad){
		if (!this.pedidoConfirmado) {
			for(int i = 0; i < cantidad; i++) {
				this.bebidasPedidas.add(bebida);
			}
		}
	}

	void pedirPlato(PlatoPrincipal plato, int cantidad){
		if (!this.pedidoConfirmado) {
			for(int i = 0; i < cantidad; i++) {
				this.platosPedidos.add(plato);
			}	
		}
	}
	void confirmarPedido() {
		this.pedidoConfirmado = true;
	}
	
	private double calcularCostoBebidas() {
		double costoTotal = 0.0;
		for(Bebida bebida : this.bebidasPedidas) {
			costoTotal += bebida.precio();
		}
		return costoTotal;
	}
	
	private double calcularCostoPlatos() {
		double costoTotal = 0.0;
		for(PlatoPrincipal plato : this.platosPedidos) {
			costoTotal += plato.precio();
		}
		return costoTotal;
	}
	
	Recibo cobrar(Tarjeta tarjeta, double propina) throws Exception {
		if(this.pedidoConfirmado) {
			double costoTotal = tarjeta.pagar(this.calcularCostoBebidas(), this.calcularCostoPlatos(), propina);
			Recibo recibo = new Recibo(costoTotal, this.bebidasPedidas, this.platosPedidos, this.proveedorDeFecha.get());
			this.registrador.registrar(costoTotal, this.proveedorDeFecha.get());
			return recibo;
		}
		else {
			throw new Exception("no está confirmado el pedido");
		}
	}
}
