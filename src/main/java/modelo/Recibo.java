package modelo;

import java.time.LocalDate;
import java.util.List;

public record Recibo(double costoTotal, List<Bebida> bebidasPedidas, 
		List<PlatoPrincipal> platosPedidos, LocalDate fecha) {
}
