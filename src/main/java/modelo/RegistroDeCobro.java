package modelo;

import java.time.LocalDate;

public interface RegistroDeCobro {
	
	public void registrar(double ganancia, LocalDate fecha);
}
