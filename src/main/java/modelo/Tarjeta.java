package modelo;

class Tarjeta {
	private double aplicarDescuento(double costoBebidas, double costoPlatos){
		double costoTotal = costoBebidas + costoPlatos;
		return costoTotal;
	}

	double pagar(double costoBebidas, double costoPLatos, double propina) throws Exception {
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;			
		return costoTotal;
	}
}
