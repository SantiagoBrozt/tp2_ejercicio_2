package modelo;

class Visa extends Tarjeta {
	private double descuentoBebidas = 0.03;
	private double descuentoPlatos = 0;
	
	private double aplicarDescuento(double costoBebidas, double costoPlatos){
		Double costoTotal = this.aplicarDescuentoBebidas(costoBebidas) + aplicarDescuentoPlatos(costoPlatos);
		return costoTotal;
	}
	
	private double aplicarDescuentoBebidas (double costo){
		costo -= costo * descuentoBebidas;
		return costo;
	}
	
	private double aplicarDescuentoPlatos (double costo){
		costo -= costo * descuentoPlatos;
		return costo;
	}
	
	double pagar(double costoBebidas, double costoPLatos, double propina) throws Exception {
		double costoTotal = this.aplicarDescuento(costoBebidas, costoPLatos);
		costoTotal += costoTotal * propina;
		return costoTotal;
	}
}
