package persistencia;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;

import modelo.RegistroDeCobro;

public class EnDataBaseRegistroDeCobro implements RegistroDeCobro{
	
	private final static String conexion = "jdbc:mysql://localhost:3306/tp2_ejercicio_2";
	private final static String usuario= "SantiagoBrozt";
	private final static String clave = "okQFuK8ohJKeeeiK";	

	@Override
	public void registrar(double ganancia, LocalDate fecha) {
	    String insertarParticipante= "INSERT INTO recibo (costo_total, fecha) "
	    		+ "VALUES (?, ?)";
	    try (Connection myConexion = DriverManager.getConnection(conexion, usuario, clave);
	    	PreparedStatement statementParticipante = myConexion.prepareStatement(insertarParticipante)) {
			//Insertar participante
	    	statementParticipante.setDouble(1, ganancia);
	    	statementParticipante.setDate(2, Date.valueOf(fecha));
	    	statementParticipante.executeUpdate();
	    }
	    catch (SQLException e) {
	    	throw new RuntimeException("Error de conexion", e);
	    }		
	}
}
