package persistencia;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;

import modelo.RegistroDeCobro;

public class EnDiscoRegistroDeCobro implements RegistroDeCobro{

	private String localPath;
	
	public EnDiscoRegistroDeCobro(String path) {
		this.localPath = path;
	}
	@Override
	public void registrar(double ganancia, LocalDate fecha) {
		String registro = fecha + " || $ " + ganancia + "\n";
		try {
			Files.write(Paths.get(this.localPath), registro.getBytes(), 
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw new RuntimeException("No se ha podido persistir", e);
		}	
	}

}
