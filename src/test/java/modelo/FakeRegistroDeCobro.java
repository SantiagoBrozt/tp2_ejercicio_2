package modelo;

import java.time.LocalDate;

public class FakeRegistroDeCobro implements RegistroDeCobro{
	private String registro = "";
	
	public boolean seRegistro(String consulta) {
		return this.registro.equals(consulta);
	}

	@Override
	public void registrar(double ganancia, LocalDate fecha) {
		String registro = fecha + " || $ " + ganancia;
		this.registro = registro;
	}
	
	
	

}
