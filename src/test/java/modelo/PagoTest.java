package modelo;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import modelo.Bebida;
import modelo.ComarcaPlus;
import modelo.MasterCard;
import modelo.Mesa;
import modelo.PlatoPrincipal;
import modelo.Recibo;
import modelo.Tarjeta;
import modelo.Visa;
import persistencia.EnDiscoRegistroDeCobro;

public class PagoTest {
	
	@Test
	public void pagoVisa() {
		//inicialización
		var fakeProveedorDeFecha = new FakeProveedorDeFecha();
		var fakeRegistro = new FakeRegistroDeCobro();
		var mesa1 = new Mesa(1, fakeRegistro, fakeProveedorDeFecha);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var visa = new Visa();
        //proceso
        mesa1.pedirPlato(p1, 2);
        mesa1.pedirPlato(p2, 1);
        mesa1.pedirBedida(b1, 5);
        mesa1.pedirBedida(b2, 3);
        mesa1.confirmarPedido();
        try {
			 var recibo = mesa1.cobrar(visa, 0.02);
		} 
        catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//verificación
        assertTrue(fakeRegistro.seRegistro("2024-04-06 || $ " + 42569.7));
	}
	
	@Test
	public void pagoMasterCard() {
		//inicialización
		var fakeProveedorDeFecha = new FakeProveedorDeFecha();
		var fakeRegistro = new FakeRegistroDeCobro();
		var mesa2 = new Mesa(2, fakeRegistro, fakeProveedorDeFecha);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var masterCard = new MasterCard();
		//proceso
        mesa2.pedirPlato(p1, 2);
        mesa2.pedirPlato(p2, 1);
        mesa2.pedirBedida(b1, 5);
        mesa2.pedirBedida(b2, 3);
        mesa2.confirmarPedido();
        try {
			var recibo = mesa2.cobrar(masterCard, 0.02);
		} 
        catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//verificación
        assertTrue(fakeRegistro.seRegistro("2024-04-06 || $ " + 43003.2));
	}
	
	@Test
	public void pagoComarcaPlus() {
		//inicialización
		var fakeProveedorDeFecha = new FakeProveedorDeFecha();
		var fakeRegistro = new FakeRegistroDeCobro();
		var mesa3 = new Mesa(3, fakeRegistro, fakeProveedorDeFecha);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000.0);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var comarcaPlus = new ComarcaPlus();
        //proceso
        mesa3.pedirPlato(p1, 2);
        mesa3.pedirPlato(p2, 1);
        mesa3.pedirBedida(b1, 5);
        mesa3.pedirBedida(b2, 3);
        mesa3.confirmarPedido();
        try {
			var recibo = mesa3.cobrar(comarcaPlus, 0.02);
		}
        catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//verificación
        assertTrue(fakeRegistro.seRegistro("2024-04-06 || $ " + 42483.0));
	}
	
	@Test
	public void pagoTarjetaCualquiera() {
		//inicialización
		var fakeProveedorDeFecha = new FakeProveedorDeFecha();
		var fakeRegistro = new FakeRegistroDeCobro();
		var mesa4 = new Mesa(4, fakeRegistro, fakeProveedorDeFecha);
		var p1= new PlatoPrincipal("Pizza Margarita", 5500);
	    var p2 = new PlatoPrincipal("Hamburguesa Clásica", 6000);
	    var b1 = new Bebida("Agua Mineral", 3000);
        var b2 = new Bebida("Refresco de Cola", 3500);
        var tarjeta = new Tarjeta();
        //proceso
        mesa4.pedirPlato(p1, 2);
        mesa4.pedirPlato(p2, 1);
        mesa4.pedirBedida(b1, 5);
        mesa4.pedirBedida(b2, 3);
        mesa4.confirmarPedido();
        try {
			var recibo = mesa4.cobrar(tarjeta, 0.02);
		}
        catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//verificación
        assertTrue(fakeRegistro.seRegistro("2024-04-06 || $ " + 43350.0));
	}
}

